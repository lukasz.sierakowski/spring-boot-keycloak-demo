package com.example.springbootkeycloakdemo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;

@RestController
@RequestMapping("/clock")
public class Controller {

    @GetMapping
    public Instant getCurrentTime() {
        return Instant.now();
    }
}
