# spring-boot-keycloak-demo

## Prerequisites
- Keycloak 19.0.3 is up and running on port `9099`
- Keycloak has imported realm configuration from file `/config/keycloak-configuration.json`


